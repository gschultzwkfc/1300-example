# Cornell University Sustainable Design (CUSD) Website

## Getting Started

* Make sure you have Grunt and all dependencies installed by running `npm install`
* Run `grunt` for development or `grunt build` for production to generate a copy of the site in the `build ` folder.
* Alternatively, `grunt watch` can be used for development, and, in addition to re-compiling files on changes, also enables a live-reload server.
